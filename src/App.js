import React from 'react';
import { connect } from 'react-redux';
import { useRoutes } from './routes';
import { Container } from 'react-bootstrap';
import NavbarContainer from './containers/NavbarContainer';

import { initialApplication } from './redux/actions/app.actions';

function App(props) {
  const { initialApplication } = props;
  React.useEffect(() => {
    initialApplication();
  }, [initialApplication]);
  const isAuth = !!props.user.currentUser;
  const routes = useRoutes(isAuth);
  return (
    <>
      {isAuth && <NavbarContainer />}
      <Container>{routes}</Container>
    </>
  );
}

const mapStateToProps = (state) => state;
const mapDisptachToProps = {
  initialApplication,
};

export default connect(mapStateToProps, mapDisptachToProps)(App);
