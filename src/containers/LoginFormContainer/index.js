import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { LoginFormComponent } from '../../components/LoginFormComponent';

import { loginClickAction } from '../../redux/actions/login.actions';

const LoginReduxFormController = reduxForm({ form: 'login' })(LoginFormComponent);

const LoginContainer = (props) => {
  const onSubmit = (formData) => props.loginClickAction(formData);

  return <LoginReduxFormController onSubmit={onSubmit} />;
};

const mapStateToProps = (state) => state;
const mapDisptachToProps = {
  loginClickAction,
};

export default connect(mapStateToProps, mapDisptachToProps)(LoginContainer);
