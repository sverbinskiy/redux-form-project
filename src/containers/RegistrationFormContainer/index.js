import { useEffect } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { useHistory } from 'react-router-dom';
import { RegistrationFormComponent } from '../../components/RegistrationFormComponent';

import { registerClickAction } from '../../redux/actions/register.actions';

const RegistrationReduxFormController = reduxForm({ form: 'register' })(RegistrationFormComponent);

const RegistrationContainer = (props) => {
  const { message } = props;
  const history = useHistory();
  useEffect(() => {
    if (message === 'User created') {
      history.push('/login');
    }
  }, [message, history]);
  const onSubmit = (formData) => props.registerClickAction(formData);
  return <RegistrationReduxFormController onSubmit={onSubmit} disabled={props.loading} />;
};

const mapStateToProps = (state) => state.app;
const mapDisptachToProps = {
  registerClickAction,
};

export default connect(mapStateToProps, mapDisptachToProps)(RegistrationContainer);
