import React from 'react';
import { connect } from 'react-redux';
import { NavbarComponent } from '../../components/NavbarComponent';

import { logoutClickAction } from '../../redux/actions/login.actions';

const NavbarContainer = (props) => {
  const { logoutClickAction } = props;
  return <NavbarComponent logoutClick={logoutClickAction} />;
};

const mapStateToProps = (state) => state.user;
const mapDispatchToProps = {
  logoutClickAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(NavbarContainer);
