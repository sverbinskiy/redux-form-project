import React from 'react';
import { Row, Col } from 'react-bootstrap';
import LoginFormContainer from '../../containers/LoginFormContainer';

const LoginPage = () => (
  <Row>
    <Col lg='6' md={{ span: 3, offset: 3 }}>
      <h2>Login on the service</h2>
      <LoginFormContainer />
    </Col>
  </Row>
);

export default LoginPage;
