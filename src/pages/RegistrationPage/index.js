import React from 'react';
import { Row, Col } from 'react-bootstrap';
import RegistrationReduxFormController from '../../containers/RegistrationFormContainer';

const RegistrationPage = () => {
  return (
    <Row>
      <Col lg='6' md={{ span: 3, offset: 3 }}>
        <h2>Register on the service</h2>
        <RegistrationReduxFormController />
      </Col>
    </Row>
  );
};

export default RegistrationPage;
