import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import RegisterPage from './pages/RegistrationPage';
import LoginPage from './pages/LoginPage';
import NavbarContainer from './containers/NavbarContainer';

export const useRoutes = (isAuth) => {
  if (isAuth) {
    return (
      <Switch>
        <Route path='/' render={() => <NavbarContainer /> && <div>Main page</div>} exact />
        <Redirect to='/' />
      </Switch>
    );
  }

  return (
    <Switch>
      <Route path='/register' render={() => <RegisterPage />} exact />
      <Route path='/login' render={() => <LoginPage />} exact />
      <Redirect to='/login' />
    </Switch>
  );
};
