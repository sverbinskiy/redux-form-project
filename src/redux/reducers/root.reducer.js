import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { appReducer } from './app.reducer';
import { userReducer } from './user.reducer';

const rootReducer = combineReducers({
  app: appReducer,
  user: userReducer,
  form: formReducer,
});

export default rootReducer;
