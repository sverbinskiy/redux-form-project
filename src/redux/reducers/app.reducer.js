import { CLEAR_MESSAGE, HIDE_LOADING, SET_MESSAGE, SHOW_LOADING } from '../types';

const initialState = {
  loading: false,
  message: '',
};

export const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_LOADING:
      return { ...state, loading: true };
    case HIDE_LOADING:
      return { ...state, loading: false };
    case SET_MESSAGE:
      return { ...state, message: action.payload.message };
    case CLEAR_MESSAGE:
      return { ...state, message: '' };
    default:
      return state;
  }
};
