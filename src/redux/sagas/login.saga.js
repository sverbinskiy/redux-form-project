import { takeEvery, put, call } from 'redux-saga/effects';
import { loginUserFetch } from '../../api/user.api';
import { remove, setItem } from '../../utils/localstorage';
import { clearMessage, hideLoading, showLoading, setMessage } from '../actions/app.actions';
import { loginAction, logoutAction } from '../actions/login.actions';
import { LOGIN_CLICK, LOGOUT_CLICK } from '../types';

const delay = (time) => new Promise((resolve) => setTimeout(resolve, time));

export function* loginWatcher() {
  yield takeEvery(LOGIN_CLICK, loginWorker);
  yield takeEvery(LOGOUT_CLICK, logoutWorker);
}

function* loginWorker({ formData }) {
  yield put(showLoading());
  const payload = yield call(loginUserFetch, formData);
  if (formData.rememberMe) {
    yield call(setItem, 'token', payload.token);
  }
  yield put(loginAction(payload.user));
  yield put(setMessage(payload.message));
  yield delay(2000);
  yield put(clearMessage());
  yield put(hideLoading());
}

function* logoutWorker() {
  yield put(showLoading());
  yield call(remove, 'token');
  yield put(logoutAction());
  yield put(hideLoading());
}
