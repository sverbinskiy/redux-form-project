import { call, put, takeEvery } from 'redux-saga/effects';
import { registerNewUserFetch } from '../../api/user.api';
import { clearMessage, hideLoading, setMessage, showLoading } from '../actions/app.actions';
import { REGISTER_CLICK } from '../types';

const delay = (time) => new Promise((resolve) => setTimeout(resolve, time));

export function* registerWatcher() {
  yield takeEvery(REGISTER_CLICK, registeWorker);
}

function* registeWorker({ formData }) {
  yield put(showLoading());
  const payload = yield call(registerNewUserFetch, formData);
  yield put(setMessage(payload));
  yield put(hideLoading());
  yield call(delay, 2000);
  yield put(clearMessage());
}
