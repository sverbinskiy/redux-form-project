import { all } from 'redux-saga/effects';
import { appWatcher } from './app.saga';
import { loginWatcher } from './login.saga';
import { registerWatcher } from './register.saga';

export default function* rootSaga() {
  yield all([appWatcher(), registerWatcher(), loginWatcher()]);
}
