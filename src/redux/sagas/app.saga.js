import { takeEvery, call, put } from 'redux-saga/effects';
import { getItem, setItem } from '../../utils/localstorage';
import { reloginUserFetch } from '../../api/user.api';
import { clearMessage, hideLoading, setMessage, showLoading } from '../actions/app.actions';
import { INITIAL } from '../types';
import { loginAction } from '../actions/login.actions';

const delay = (time) => new Promise((resolve) => setTimeout(resolve, time));

export function* appWatcher() {
  yield takeEvery(INITIAL, initialWorker);
}

function* initialWorker() {
  const token = yield call(getItem, 'token');
  if (token) {
    yield put(showLoading());
    const payload = yield call(reloginUserFetch, token);
    if (payload.token) {
      yield call(setItem, 'token', payload.token);
      yield put(loginAction(payload.user));
    }
    yield put(setMessage(payload.message));
    yield delay(2000);
    yield put(clearMessage());
    yield put(hideLoading());
  }
}
