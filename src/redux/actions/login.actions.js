import { LOGIN, LOGIN_CLICK, LOGOUT, LOGOUT_CLICK } from '../types';

export const loginClickAction = (formData) => ({ type: LOGIN_CLICK, formData });
export const loginAction = (payload) => ({ type: LOGIN, payload });
export const logoutClickAction = () => ({ type: LOGOUT_CLICK });
export const logoutAction = () => ({ type: LOGOUT });
