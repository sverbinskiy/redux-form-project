import { CLEAR_MESSAGE, HIDE_LOADING, INITIAL, SET_MESSAGE, SHOW_LOADING } from '../types';

export const initialApplication = () => ({ type: INITIAL });
export const showLoading = () => ({ type: SHOW_LOADING });
export const hideLoading = () => ({ type: HIDE_LOADING });
export const setMessage = (payload) => ({ type: SET_MESSAGE, payload });
export const clearMessage = () => ({ type: CLEAR_MESSAGE });
