import { REGISTER_CLICK } from '../types';

export const registerClickAction = (formData) => ({ type: REGISTER_CLICK, formData });
