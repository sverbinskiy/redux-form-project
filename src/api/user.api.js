import axios from 'axios';

export async function registerNewUserFetch(body) {
  try {
    const response = await axios.post('/api/user/register', body);
    return response.data;
  } catch (error) {
    return error.response.data;
  }
}

export async function loginUserFetch(body) {
  try {
    const response = await axios.post('/api/user/login', body);
    return response.data;
  } catch (error) {
    return error.response.data;
  }
}

export async function reloginUserFetch(token) {
  try {
    const response = await axios.get('/api/user/relogin', {
      headers: { Authorization: token },
    });
    return response.data;
  } catch (error) {
    return error.response.data;
  }
}
