import React from 'react';
import { Form, FormCheck, FormControl } from 'react-bootstrap';

export const ReduxFormControl = ({ input, meta, ...props }) => {
  return (
    <Form.Group controlId={props.name}>
      <Form.Label>{props.label}</Form.Label>
      <FormControl {...props} {...input} isInvalid={meta.error && meta.touched} />
      <Form.Control.Feedback type='invalid'>{meta.error}</Form.Control.Feedback>
      {props.text ? <Form.Text muted>{props.text}</Form.Text> : ''}
    </Form.Group>
  );
};

export const ReduxFormCheck = ({ input, meta, ...props }) => (
  <Form.Group controlId={props.name}>
    <FormCheck {...props} {...input} />
  </Form.Group>
);
