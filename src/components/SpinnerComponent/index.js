import React from 'react';
import { Spinner } from 'react-bootstrap';

export const SpinnerComponent = () => (
  <Spinner animation={'border'} role={'status'}>
    <span className='rs-only'>Loading...</span>
  </Spinner>
);
