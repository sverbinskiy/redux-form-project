import React from 'react';
import { Link } from 'react-router-dom';
import { Field } from 'redux-form';
import { Form, Button } from 'react-bootstrap';
import { ReduxFormControl } from '../ReduxFormControl';
import { email, matchPassword, maxLength64, minLength8, required } from '../../utils/validators';

export const RegistrationFormComponent = (props) => (
  <Form onSubmit={props.handleSubmit}>
    <Field
      name={'name'}
      placeholder={'Enter your name'}
      label={'Name'}
      component={ReduxFormControl}
      validate={[required]}
    />
    <Field
      name={'email'}
      type={'email'}
      label={'Email'}
      placeholder={'Enter your email'}
      component={ReduxFormControl}
      validate={[required, email]}
    />
    <Field
      name={'password'}
      type={'password'}
      label={'Password'}
      text={'Must be 8 - 64 characters long'}
      placeholder={'Enter your password'}
      component={ReduxFormControl}
      validate={[required, minLength8, maxLength64]}
    />
    <Field
      name={'confirmPassword'}
      type={'password'}
      label={'Confirm password'}
      placeholder={'Confirm your password'}
      component={ReduxFormControl}
      validate={[required, matchPassword]}
    />
    <Button type={'submit'} variant='outline-primary' block disabled={props.loading}>
      Registration
    </Button>
    <p style={{ marginTop: 10 }}>
      Have an account? <Link to='/login'>Login.</Link>
    </p>
  </Form>
);
