import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, Form, Button, Container } from 'react-bootstrap';

export const NavbarComponent = (props) => {
  return (
    <Navbar bg='light' variant='light'>
      <Container>
        <Nav className='mr-auto'>
          <Link to='/' style={{ marginRight: 20 }}>
            Home
          </Link>
          <Link to='/posts' style={{ marginRight: 20 }}>
            Posts
          </Link>
          <Link to='/create' style={{ marginRight: 20 }}>
            Create new post
          </Link>
        </Nav>
        <Form inline>
          <Button variant='outline-primary'>My account</Button>
          <Button
            style={{ marginLeft: 10 }}
            variant='outline-secondary'
            onClick={() => props.logoutClick()}
          >
            Log out
          </Button>
        </Form>
      </Container>
    </Navbar>
  );
};
