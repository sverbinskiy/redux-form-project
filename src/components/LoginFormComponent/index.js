import React from 'react';
import { Link } from 'react-router-dom';
import { Field } from 'redux-form';
import { Form, Button } from 'react-bootstrap';
import { ReduxFormControl, ReduxFormCheck } from '../ReduxFormControl';
import { email, maxLength64, minLength8, required } from '../../utils/validators';

export const LoginFormComponent = (props) => (
  <Form onSubmit={props.handleSubmit}>
    <Field
      name={'email'}
      type={'email'}
      label={'Email'}
      placeholder={'Enter your email'}
      component={ReduxFormControl}
      validate={[required, email]}
    />
    <Field
      name={'password'}
      type={'password'}
      label={'Password'}
      placeholder={'Enter your password'}
      component={ReduxFormControl}
      validate={[required, minLength8, maxLength64]}
    />
    <Field name={'rememberMe'} label='Remember me' component={ReduxFormCheck} />
    <Button type={'submit'} variant='outline-primary' block>
      Login
    </Button>
    <p style={{ marginTop: 10 }}>
      Don't have an account? <Link to='/register'>Register now.</Link>
    </p>
  </Form>
);
