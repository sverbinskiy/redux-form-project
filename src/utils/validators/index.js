const maxLengthCreator = (max) => (value) =>
  value && value.length <= max ? undefined : `Max password length ${max} characters`;
const minLengthCreator = (min) => (value) =>
  value && value.length >= min ? undefined : `Mip password length ${min} characters`;
const matchCreator = (matchName) => (value, allValue, props) =>
  value !== allValue[matchName] ? `This field must match with ${matchName} field` : undefined;

export const required = (value) => (value ? undefined : 'Required field');
export const email = (value) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined;
export const maxLength64 = maxLengthCreator(64);
export const minLength8 = minLengthCreator(8);
export const matchPassword = matchCreator('password');
