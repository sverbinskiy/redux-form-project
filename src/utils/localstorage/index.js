export const setItem = (name, data) => localStorage.setItem(name, data);
export const getItem = (name) => {
  const data = localStorage.getItem(name);
  return data;
};
export const remove = (name) => localStorage.removeItem(name);
